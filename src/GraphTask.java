import java.util.*;

public class GraphTask {

   public static void main (String[] args) {
      GraphTask a = new GraphTask();
      a.run();
      //throw new RuntimeException ("Nothing implemented yet!"); // delete this
   }

   public void run() {
      Graph g = new Graph ("G");
       System.out.println(" I Test: n = -1:");
       g.createRandomSimpleGraph (2, 1); //create random graph with n vertices and m edges
       g.printMatrix(g.createAdjMatrix());
       System.out.println (g); //print generated graph
       try {  g.graphToPow(-1); } catch (Exception e) {
           System.out.println("Viga: " + e + "\n"); }
       System.out.println("II Test: n = 0:");
       g.createRandomSimpleGraph (4, 3); //create random graph with n vertices and m edges
       g.printMatrix(g.createAdjMatrix());
       System.out.println (g); //print generated graph
       try {  g.graphToPow(0); } catch (Exception e) {
           System.out.println("Viga: " + e + "\n"); }
       System.out.println(" III Test n = 1:");
       g.createRandomSimpleGraph (3, 2); //create random graph with n vertices and m edges
       g.printMatrix(g.createAdjMatrix());
       System.out.println (g); //print generated graph
       try {  g.graphToPow(1); } catch (Exception e) {
           System.out.println("Viga: " + e + "\n"); }
       System.out.println(" IV Test n = 2:");
       g.createRandomSimpleGraph (3, 3); //create random graph with n vertices and m edges
       g.printMatrix(g.createAdjMatrix());
       System.out.println (g); //print generated graph
       try {  g.graphToPow(2); } catch (Exception e) {
           System.out.println("Viga: " + e + "\n"); }
       System.out.println(" V Test n = 3:");
       g.createRandomSimpleGraph (4, 4); //create random graph with n vertices and m edges
       g.printMatrix(g.createAdjMatrix());
       System.out.println (g); //print generated graph
       try {  g.graphToPow(3); } catch (Exception e) {
           System.out.println("Viga: " + e + "\n"); }
       System.out.println(" VI Test n = 5:");
       g.createRandomSimpleGraph (5, 4); //create random graph with n vertices and m edges
       g.printMatrix(g.createAdjMatrix());
       System.out.println (g); //print generated graph
       try {  g.graphToPow(5); } catch (Exception e) {
           System.out.println("Viga: " + e + "\n"); }

   }
   class Vertex {

      private String id;
      private Vertex next;
      private Arc first;
      private int info = 0;

      Vertex (String s, Vertex v, Arc e) {
         id = s;
         next = v;
         first = e;
      }

      Vertex (String s) {
         this (s, null, null);
      }

      @Override
      public String toString() {
         return id;
      }


   }


   class Arc {

      private String id;
      private Vertex target;
      private Arc next;
      private int info = 0;

      Arc (String s, Vertex v, Arc a) {
         id = s;
         target = v;
         next = a;
      }

      Arc (String s) {
         this (s, null, null);
      }

      @Override
      public String toString() {
         return id;
      }


   }


   class Graph {

      private String id;
      private Vertex first;
      private int info = 0;

      Graph (String s, Vertex v) {
         id = s;
         first = v;
      }

      Graph (String s) {
         this (s, null);
      }

      @Override
      public String toString() {
         String nl = System.getProperty ("line.separator");
         StringBuffer sb = new StringBuffer (nl);
         sb.append (id);
         sb.append (nl);
         Vertex v = first;
         while (v != null) {
            sb.append (v.toString());
            sb.append (" -->");
            Arc a = v.first;
            while (a != null) {
               sb.append (" ");
               sb.append (a.toString());
               sb.append (" (");
               sb.append (v.toString());
               sb.append ("->");
               sb.append (a.target.toString());
               sb.append (")");
               a = a.next;
            }
            sb.append (nl);
            v = v.next;
         }
         return sb.toString();
      }

      public Vertex createVertex (String vid) {
         Vertex res = new Vertex (vid);
         res.next = first;
         first = res;
         return res;
      }

      public Arc createArc (String aid, Vertex from, Vertex to) {
         Arc res = new Arc (aid);
         res.next = from.first;
         from.first = res;
         res.target = to;
         return res;
      }

      /**
       * Create a connected undirected random tree with n vertices.
       * Each new vertex is connected to some random existing vertex.
       * @param n number of vertices added to this graph
       */
      public void createRandomTree (int n) {
         if (n <= 0)
            return;
         Vertex[] varray = new Vertex [n];
         for (int i = 0; i < n; i++) {
            varray [i] = createVertex ("v" + String.valueOf(n-i));
            if (i > 0) {
               int vnr = (int)(Math.random()*i);
               createArc ("a" + varray [vnr].toString() + "_"
                  + varray [i].toString(), varray [vnr], varray [i]);
               createArc ("a" + varray [i].toString() + "_"
                  + varray [vnr].toString(), varray [i], varray [vnr]);
            } else {}
         }
      }

      /**
       * Create an adjacency matrix of this graph.
       * Side effect: corrupts info fields in the graph
       * @return adjacency matrix
       */
      public int[][] createAdjMatrix() {
         info = 0;
         Vertex v = first;
         while (v != null) {
            v.info = info++;
            v = v.next;
         }
         int[][] res = new int [info][info];
         v = first;
         while (v != null) {
            int i = v.info;
            Arc a = v.first;
            while (a != null) {
               int j = a.target.info;
               res [i][j]++;
               a = a.next;
            }
            v = v.next;
         }
         return res;
      }

      /**
       * Create a connected simple (undirected, no loops, no multiple
       * arcs) random graph with n vertices and m edges.
       * @param n number of vertices
       * @param m number of edges
       */
      public void createRandomSimpleGraph (int n, int m) {
         if (n <= 0)
            return;
         if (n > 2500)
            throw new IllegalArgumentException ("Too many vertices: " + n);
         if (m < n-1 || m > n*(n-1)/2)
            throw new IllegalArgumentException
               ("Impossible number of edges: " + m);
         first = null;
         createRandomTree (n);       // n-1 edges created here
         Vertex[] vert = new Vertex [n];
         Vertex v = first;
         int c = 0;
         while (v != null) {
            vert[c++] = v;
            v = v.next;
         }
         int[][] connected = createAdjMatrix();
         int edgeCount = m - n + 1;  // remaining edges
         while (edgeCount > 0) {
            int i = (int)(Math.random()*n);  // random source
            int j = (int)(Math.random()*n);  // random target
            if (i==j)
               continue;  // no loops
            if (connected [i][j] != 0 || connected [j][i] != 0)
               continue;  // no multiple edges
            Vertex vi = vert [i];
            Vertex vj = vert [j];
            createArc ("a" + vi.toString() + "_" + vj.toString(), vi, vj);
            connected [i][j] = 1;
            createArc ("a" + vj.toString() + "_" + vi.toString(), vj, vi);
            connected [j][i] = 1;
            edgeCount--;  // a new edge happily created
         }
      }

      // TODO!!! Your Graph methods here!
       /**
        *10. Koostada meetod etteantud graafi n astme leidmiseks (n>=0) kasutades korrutamist
        * Rises generated graph to power n
        * @param n  power number
        */
      public void graphToPow(int n) {
          if (n<0){
              throw new IllegalArgumentException ("Astendaja peab olema >= 0 !");
          } else if(n>createAdjMatrix().length) {
              System.out.println("Astendaja on suurem graafi tippude arvust! Ei leidu ühtegi teed pikkusega "+n);
              printAnswer(n,matrixZero(createAdjMatrix().length));
          } else if (n==0) {
              printAnswer(n,matrixPzero(createAdjMatrix().length));
          }else if(n==1){
              printAnswer(n, createAdjMatrix());
          }else{
              printAnswer(n,(matrixPow(createAdjMatrix(),n)));
          }

      }
       /**
        * Create an  undirected random tree with n=newAdj.length vertices.
        * @param newAdj  given 2D array
        */
       public void createResultTree (int [][] newAdj){
           int n=newAdj.length;
           if (n <= 0)
               return;
           Vertex[] varray = new Vertex [n];
           for (int i = 0; i < n; i++) {
               varray [i] = createVertex ("v" + String.valueOf(n-i));
       }
       }
       /**
        * Create a connected simple
        * graph from given adjacency matrix.
        * @param newAdj  given 2D array
        */
       public void createResultGraph (int[][]newAdj) { //create Graph from  adj matrix
           int n=newAdj.length;
           first = null;
           createResultTree(newAdj);
           Vertex[] vert = new Vertex [n];
           Vertex v = first;
           int c = 0;
           while (v != null) {
               vert[c++] = v;
               v = v.next;
           }
           for (int i = n-1; i > -1; i--) {
               for (int j = n-1; j >-1 ; j--) {
                   if (newAdj[i][j]>0){
                       Vertex vi = vert [i];
                       Vertex vj = vert [j];
                       createArc ("a" + vi.toString() + "_" + vj.toString(), vi, vj);

               } else{}

           }
           }
       }
       /**
        * Print results to console
        * @param m power number
        * @param answer result adjancency matrix
        */
       public void printAnswer (int m,int [][] answer){ //prints result graph
           System.out.println();
           System.out.println("Graaf astmes  "+ m +":");
           System.out.println();
           printMatrix(answer);
           Graph r = new Graph ("R");
           r.createResultGraph(answer);//create result graph from adj matrix
           System.out.println(r);
       }
       /**
        * Raise matrix adj to power n
        * @param adj given  matrix
        * @param n power number
        * @return  given matrix in power n
        */
       public int[][] matrixPow(int [][]adj,int n){ //rises adj matrix to power n
           int sum=0;
           int m=adj.length;
           int[][]first=new int[m][m];
           int[][]second=new int[m][m];
           for (int i = 0; i < m; i++) { // store values to second array
               for (int j = 0; j < m; j++) {
                   second[i][j]=adj[i][j];
               }
           }
           int[][]result=new int[m][m];
           first=adj;
           while (n>1) { //rise array to power n using multiplication of first and second array n times
               for (int i = 0; i < adj.length; i++) {
                   for (int j = 0; j < adj.length; j++) {
                       for (int k = 0; k < adj.length; k++) {
                           sum = sum+(first[i][k]*second[k][j]); // array multiplication
                       }
                       result[i][j] = sum;
                       sum=0;
                   }
               }
               for (int i = 0; i < m; i++) { // store new values to first array
                   for (int j = 0; j < m; j++) {
                       first[i][j]=result[i][j];
                   }
               }

               n--;
           }
           return result;
       }
       /**
        * Raise matrix to power 0
        * @param m matrix length
        * @return  given matrix in power 0
        */
       public int[][]  matrixPzero(int m){//returns matrix in pow 0
           int result[][]=new int[m][m];
           for (int i = 0; i <m ; i++) {
               for (int j = 0; j < m; j++) {
                   if (i==j){
                       result[i][j]=1;
                   }
               }
           }
           return result;

       }
       /**
        * Generate empty matrix
        * @param m matrix length
        * @return  empty matrix
        */
       public int[][]  matrixZero(int m){ //returns empty matrix
           int result[][]=new int[m][m];
           for (int i = 0; i <m ; i++) {
               for (int j = 0; j < m; j++) {
                       result[i][j]=0;
               }
           }
           return result;

       }
       /**
        * Print adjacency matrix to console
        * @param matrix matrix to print
        */
       public void printMatrix (int [][] matrix){
           System.out.println();
           System.out.println("Külgnevusmaatriks:");
           int[][] adj=new int[matrix.length][matrix.length];
           for (int i = 0; i <matrix.length ; i++) {
               for (int j = 0; j <matrix.length ; j++) {
                   if(matrix[i][j]>0){
                   adj[i][j]=1;
                   }else{
                       adj[i][j]=0;
                   }
               }
           }
           for (int[] num:adj) {
               System.out.println(Arrays.toString(num));

           }
       }
   }
}

